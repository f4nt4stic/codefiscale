import Foundation
import Kitura
import LoggerAPI
import Configuration
import CloudEnvironment
import KituraContracts
import Health

//http://localhost:8080/user?name=YourName&surname=YourSurname&female=false&date=YYYY-MM-DD&province=YourProvince&city=YourCity

public let projectPath = ConfigurationManager.BasePath.project.path

public struct Query: QueryParams {
    let name: String
    let surname: String
    let female: Bool
    let date: String
    let province: String
    let city: String
}


private func getUsers(query: Query, completion: @escaping([String:String]?, RequestError?) -> Void) -> Void {
    let name = query.name
    let surname = query.surname
    let female = query.female
    let date = query.date
    let province = query.province
    let city = query.city
    
    if (name == "" || surname == "" || date == "" || province == "" || city == ""){
        completion(["error":"empty arguments"], nil)
        return
    }
    
    let cfCalc = CFCalc()

    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    guard let dates: Date = dateFormatter.date(from: date) else {
        completion(["error":"invalid date format"], nil)
        return
    }
    
    let codice = cfCalc.CFCalc(name: name, surname: surname, female: female, date: dates, province: province, city: city)
    
    let codiceFiscale: [String:String] = ["cf":codice]
    
    completion(codiceFiscale, nil)
}

public class App {
    let router = Router()
    let cloudEnv = CloudEnv()

    public init() throws {
        // Run the metrics initializer
        initializeMetrics(router: router)
    }

    func postInit() throws {
        router.get("/user", handler: getUsers)
    }

    public func run() throws {
        try postInit()
        Kitura.addHTTPServer(onPort: cloudEnv.port, with: router)
        Kitura.run()
    }
}
